package com.br.sensedia.oracle.boilerplate.user.infrastructure.ports;

import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearch;
import org.springframework.data.domain.Page;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {
    User create(@Valid @NotNull User user);

    void delete(@NotNull Long id);

    User update(@Valid @NotNull User user, @NotNull Long id);

    User findById(@NotNull Long id);

    Page<User> findAll(@Valid @NotNull UserSearch userSearch);

}

