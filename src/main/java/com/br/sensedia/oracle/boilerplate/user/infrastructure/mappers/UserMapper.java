package com.br.sensedia.oracle.boilerplate.user.infrastructure.mappers;

import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
  User toUser(UserCreationDto userCreationDto);

  User toUser(UserUpdateDto userUpdateDto);

  UserDto toUserDto(User user);

  List<UserDto> toUserDtos(List<User> users);
}
