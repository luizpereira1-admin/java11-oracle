package com.br.sensedia.oracle.boilerplate.user.infrastructure.http;

import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearch;
import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearchBuild;

import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.lang.String.valueOf;

import com.br.sensedia.oracle.boilerplate.commons.converters.InstantConverter;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.mappers.UserMapper;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.ApplicationPort;


import static com.br.sensedia.oracle.boilerplate.commons.headers.DefaultHeader.HEADER_ACCEPT_RANGE;
import static com.br.sensedia.oracle.boilerplate.commons.headers.DefaultHeader.HEADER_CONTENT_RANGE;

@RestController
@RequestMapping("/users")
public class UserController {

    private final ApplicationPort userApplication;
    private final UserMapper userMapper;
    private final InstantConverter instantConverter;

    @Value("${app.repository.maximumLimit}")
    private int maximumLimit;

    @Autowired
    public UserController(
            ApplicationPort userApplication, UserMapper userMapper, InstantConverter instantConverter) {
        this.userApplication = userApplication;
        this.userMapper = userMapper;
        this.instantConverter = instantConverter;
    }

    @PostMapping
    public ResponseEntity<UserDto> create(@Valid @RequestBody UserCreationDto userCreation) {
        User user = userApplication.create(userMapper.toUser(userCreation));

        UserDto userDto = userMapper.toUserDto(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(userDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Valid @PathVariable Long id) {
        userApplication.delete(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> update(
            @PathVariable Long id, @Valid @RequestBody UserUpdateDto userUpdateDto) {
        User user = userApplication.update(userMapper.toUser(userUpdateDto), id);

        UserDto userDto = userMapper.toUserDto(user);

        return ResponseEntity.ok(userDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> get(@Valid @PathVariable Long id) {
        User user = userApplication.findById(id);

        UserDto userResponse = userMapper.toUserDto(user);

        return ResponseEntity.ok(userResponse);
    }


    @GetMapping
    public ResponseEntity<List<UserDto>> getAll(
            @RequestParam(value = "status", required = false) final String status,
            @RequestParam(value = "name", required = false) final String name,
            @RequestParam(value = "email", required = false) final String email,
            @RequestParam(value = "created_at_start", required = false) final String createdAtStart,
            @RequestParam(value = "created_at_end", required = false) final String createdAtEnd,
            @RequestParam(value = "sort", required = false, defaultValue = "name") String sort,
            @RequestParam(value = "sort_type", required = false, defaultValue = "asc") String sortType,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "limit", required = false) Integer limit) {

        UserSearch userSearch =
                UserSearchBuild.builder()
                        .status(status)
                        .name(name)
                        .email(email)
                        .createdAtStart(instantConverter.toInstant(createdAtStart))
                        .createdAtEnd(instantConverter.toInstant(createdAtEnd))
                        .sort(sort)
                        .sortType(sortType)
                        .page(page)
                        .limit(limit)
                        .build();

        Page<User> userSearchResponse = userApplication.findAll(userSearch);

        List<UserDto> response = userMapper.toUserDtos(userSearchResponse.getContent());

        return ResponseEntity.ok()
                .header(HEADER_CONTENT_RANGE, valueOf(userSearchResponse.getTotalElements()))
                .header(HEADER_ACCEPT_RANGE, valueOf(maximumLimit))
                .body(response);
    }

}
