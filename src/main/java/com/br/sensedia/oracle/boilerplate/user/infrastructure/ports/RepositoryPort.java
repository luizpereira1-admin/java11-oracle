package com.br.sensedia.oracle.boilerplate.user.infrastructure.ports;

import com.br.sensedia.oracle.boilerplate.user.infrastructure.repository.AdvancedUserSearch;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryPort extends CrudRepository<User, Long>, AdvancedUserSearch {}

