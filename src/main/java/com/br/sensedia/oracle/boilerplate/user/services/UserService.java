package com.br.sensedia.oracle.boilerplate.user.services;

import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.Instant;
import com.br.sensedia.oracle.boilerplate.commons.errors.exceptions.NotFoundException;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.UserStatus;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.ApplicationPort;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.RepositoryPort;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.AmqpPort;

import javax.validation.*;
import javax.validation.constraints.NotNull;

@Service
@Transactional
@Validated
public class UserService implements ApplicationPort {

    private final AmqpPort amqpPort;
    private final RepositoryPort repository;

    @Autowired
    public UserService(AmqpPort amqpPort, RepositoryPort repository){
        this.amqpPort = amqpPort;
        this.repository = repository;
    }

    @Override
    public User create(@Valid @NotNull User user){
        user.setCreatedAt(Instant.now());
        user.setStatus(UserStatus.ACTIVE);

        repository.save(user);
        amqpPort.notifyUserCreation(user);

        return user;
    }

    @Override
    public void delete(@NotNull Long id) throws ResourceNotFoundException {
        User user = findById(id);

        repository.delete(user);
        amqpPort.notifyUserDeletion(user);
    }

    @Override
    public User update(@Valid @NotNull User userForUpdate, @NotNull Long id) throws ResourceNotFoundException {
        User user = findById(id);

        user.setName(userForUpdate.getName());
        user.setEmail(userForUpdate.getEmail());

        if (userForUpdate.getStatus() != null) user.setStatus(userForUpdate.getStatus());

        user.setUpdatedAt(Instant.now());

        repository.save(user);
        amqpPort.notifyUserUpdate(user);

        return user;
    }

    @Override
    public User findById(@NotNull Long id) throws ResourceNotFoundException {
        return repository.findById(id)
                .orElseGet(
                        () -> {
                            throw new NotFoundException("User not found");
                        });
    }

    @Override
    public Page<User> findAll(@Valid @NotNull UserSearch userSearch) {
        return repository.findAll(userSearch);
    }
}
