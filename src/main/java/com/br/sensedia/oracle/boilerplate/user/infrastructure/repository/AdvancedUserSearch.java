package com.br.sensedia.oracle.boilerplate.user.infrastructure.repository;


import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearch;
import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearchResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface AdvancedUserSearch {

    Page<User> findAll(UserSearch userSearch);
}
