package com.br.sensedia.oracle.boilerplate.user.infrastructure.mappers;

import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.mappers.UserMapper;

public abstract class UserDecoratorMapper implements UserMapper {

  @Autowired
  @Qualifier("delegate")
  private UserMapper delegate;

  @Override
  public User toUser(UserUpdateDto userUpdateDto) {
    validateStatus(userUpdateDto);
    return delegate.toUser(userUpdateDto);
  }

  private void validateStatus(UserUpdateDto userUpdateDto) {
    UserStatus.fromValue(userUpdateDto.getStatus());
  }
}
