package com.br.sensedia.oracle.boilerplate.user.infrastructure.repository;

import com.br.sensedia.oracle.boilerplate.user.domain.search.SortType;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.repository.AdvancedUserSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;

import java.util.ArrayList;
import java.util.List;

import com.br.sensedia.oracle.boilerplate.commons.errors.exceptions.PreConditionException;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearch;
import com.br.sensedia.oracle.boilerplate.user.domain.search.UserSearchResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;

import org.springframework.data.jpa.domain.Specification;
import static org.springframework.data.jpa.domain.Specification.where;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.repository.UserSpecification;
import java.util.List;
import java.util.Locale;

public class AdvancedUserSearchImpl implements AdvancedUserSearch {


    private final UserRepository userRepository;
    private final EntityManager entityManager;

    @Value("${app.repository.maximumLimit}")
    private int maximumLimit;

    @Value("${app.repository.defaultLimit}")
    private int defaultLimit;

    @Value("${app.repository.defaultSort}")
    private String defaultSort;

    @Autowired
    public AdvancedUserSearchImpl(EntityManager entityManager, UserRepository userRepository) {
        this.entityManager = entityManager;
        this.userRepository = userRepository;
    }

    @Override
    public Page<User> findAll(UserSearch userSearch) {
        int limit = userSearch.getLimit() == null ? defaultLimit : userSearch.getLimit();

        SortType sortType = userSearch.getSortType() == null ? SortType.ASC : userSearch.getSortType();
        String sortBy = userSearch.getSort() == null ? com.br.sensedia.oracle.boilerplate.user.domain.search.Sort.CREATED_AT.toString().toLowerCase(Locale.ROOT) : userSearch.getSort().getFieldName();
        if (limit > maximumLimit) {
            throw new PreConditionException(
                    "The 'limit' field is greater than the configured maximum limit [" + maximumLimit + "]");
        }

        int page = userSearch.getPage() > 0 ? userSearch.getPage() - 1 : 0;
        PageRequest pageRequest = sortType == SortType.ASC ?
                PageRequest.of(page, limit, Sort.by(sortBy).ascending()) :
                    PageRequest.of(page, limit, Sort.by(sortBy).descending());


            if(hasParam(userSearch)){
                if(withStartAndEnd(userSearch)){
                    return userRepository.findAll(where(UserSpecification.name(userSearch.getName()))
                                    .or(UserSpecification.email(userSearch.getEmail()))
                                    .or(UserSpecification.status(userSearch.getStatus()))
                                    .or(UserSpecification.createdAtStart(userSearch.getCreatedAtStart()))
                                    .and(UserSpecification.createdAtEnd(userSearch.getCreatedAtEnd())),
                            pageRequest);
                }else {
                    return userRepository.findAll(where(UserSpecification.name(userSearch.getName()))
                                    .or(UserSpecification.email(userSearch.getEmail()))
                                    .or(UserSpecification.status(userSearch.getStatus()))
                                    .or(UserSpecification.createdAtStart(userSearch.getCreatedAtStart()))
                                    .or(UserSpecification.createdAtEnd(userSearch.getCreatedAtEnd())),
                            pageRequest);
                }


            }

            return userRepository.findAll(pageRequest);

    }

    private boolean hasParam(UserSearch userSearch){

        return StringUtils.isNotBlank(userSearch.getEmail())
                || StringUtils.isNotBlank(userSearch.getName())
                || userSearch.getStatus() != null
                || userSearch.getCreatedAtEnd() != null
                || userSearch.getCreatedAtStart() != null;
    }

    private boolean withStartAndEnd(UserSearch userSearch){
        if(userSearch.getCreatedAtEnd() != null && userSearch.getCreatedAtStart() != null){
            return true;
        }
        return false;
    }


}

