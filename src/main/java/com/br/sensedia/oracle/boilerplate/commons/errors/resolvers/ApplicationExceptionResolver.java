package com.br.sensedia.oracle.boilerplate.commons.errors.resolvers;

import com.br.sensedia.oracle.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.br.sensedia.oracle.boilerplate.commons.errors.exceptions.ApplicationException;
import org.springframework.stereotype.Service;

@Service
public class ApplicationExceptionResolver implements Resolver<ApplicationException> {

  @Override
  public DefaultErrorResponse getErrorResponse(ApplicationException e) {
    return e.getDefaultErrorResponse();
  }
}
