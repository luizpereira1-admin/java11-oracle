package com.br.sensedia.oracle.boilerplate.user.domain.search;

import org.apache.commons.lang3.StringUtils;
import com.br.sensedia.oracle.boilerplate.commons.errors.exceptions.BadRequestException;

public enum SortType {
    ASC("ascending"),
    DESC("descending");

    private String value;

    SortType(String value) {
        this.value = value;
    }

    public static SortType fromValue(String value) {
        if (StringUtils.isBlank(value)) return null;

        for (SortType sortType : SortType.values()) {
            if (sortType.name().equalsIgnoreCase(value)) {
                return sortType;
            }
        }

        throw new BadRequestException(
                "Invalid sort type [" + value + "], accepted values: [asc, desc]");
    }

    public String getValue() {
        return value;
    }
}
