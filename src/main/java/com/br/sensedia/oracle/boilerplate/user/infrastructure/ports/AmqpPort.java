package com.br.sensedia.oracle.boilerplate.user.infrastructure.ports;

import com.br.sensedia.oracle.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import org.springframework.stereotype.Component;

@Component
public interface AmqpPort {

  void notifyUserCreation(User user);

  void notifyUserDeletion(User user);

  void notifyUserOperationError(DefaultErrorResponse errorResponse);

  void notifyUserUpdate(User user);
}
