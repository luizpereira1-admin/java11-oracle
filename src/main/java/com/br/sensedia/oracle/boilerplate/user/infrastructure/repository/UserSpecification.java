package com.br.sensedia.oracle.boilerplate.user.infrastructure.repository;

import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.UserStatus;
import org.springframework.data.jpa.domain.Specification;

import java.time.Instant;
import java.util.Locale;


public class UserSpecification {
    public static Specification<User> name(String name) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("name"), "%" + name + "%");
    }

    public static Specification<User> email(String email) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("email"), "%" + email + "%");
    }

    public static Specification<User> createdAtStart(Instant createdAtStart){
        return ((root, criteriaQuery, criteriaBuilder) ->
                 criteriaBuilder.greaterThan(root.get("createdAt"), createdAtStart));
    }

    public static Specification<User> createdAtEnd(Instant createdAtEnd){
        return (root, criteriaQuery, criteriaBuilder) ->
                    criteriaBuilder.lessThan(root.get("createdAt"), createdAtEnd);
    }

    public static Specification<User> status(UserStatus status) {
        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("status"), status));
    }

}