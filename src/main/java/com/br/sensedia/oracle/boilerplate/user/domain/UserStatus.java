package com.br.sensedia.oracle.boilerplate.user.domain;

import org.apache.commons.lang3.StringUtils;
import com.br.sensedia.oracle.boilerplate.commons.errors.exceptions.BadRequestException;

public enum UserStatus {
    ACTIVE,
    DISABLE;

    public static UserStatus fromValue(String value) {
        if (StringUtils.isBlank(value)) return null;

        for (UserStatus status : UserStatus.values()) {
            if (status.name().equalsIgnoreCase(value)) {
                return status;
            }
        }

        throw new BadRequestException(
                "Invalid status [" + value + "], accepted values: [active, disable]");
    }
}
