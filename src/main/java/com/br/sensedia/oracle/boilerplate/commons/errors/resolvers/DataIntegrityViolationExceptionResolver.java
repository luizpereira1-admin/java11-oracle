package com.br.sensedia.oracle.boilerplate.commons.errors.resolvers;

import com.br.sensedia.oracle.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DataIntegrityViolationExceptionResolver extends BaseResolver
        implements Resolver<DataIntegrityViolationException> {

    @Override
    public DefaultErrorResponse getErrorResponse(DataIntegrityViolationException e) {
        String defaultMessage;
        defaultMessage = e.getCause().getMessage();

        return new DefaultErrorResponse(
                HttpStatus.BAD_REQUEST, defaultMessage);
    }
}