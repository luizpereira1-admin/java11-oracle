package com.br.sensedia.oracle.boilerplate.commons.errors.resolvers;

import com.br.sensedia.oracle.boilerplate.commons.errors.domains.DefaultErrorResponse;

public interface Resolver<T extends Throwable> {
  DefaultErrorResponse getErrorResponse(T e);
}
