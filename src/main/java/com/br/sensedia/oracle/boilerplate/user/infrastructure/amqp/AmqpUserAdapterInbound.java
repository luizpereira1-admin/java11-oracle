package com.br.sensedia.oracle.boilerplate.user.infrastructure.amqp;

import com.br.sensedia.oracle.boilerplate.commons.beans.BeanValidator;
import com.br.sensedia.oracle.boilerplate.commons.errors.exceptions.BadRequestException;
import com.br.sensedia.oracle.boilerplate.commons.errors.resolvers.ExceptionResolver;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.amqp.config.BindConfig;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.amqp.config.BrokerInput;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserDeletionDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.mappers.UserMapper;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.AmqpPort;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.ApplicationPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;


@EnableBinding(BrokerInput.class)
public class AmqpUserAdapterInbound {

    private final ApplicationPort applicationPort;
    private final UserMapper userMapper;
    private final AmqpPort amqpPort;
    private final ExceptionResolver exceptionResolver;

    @Autowired
    public AmqpUserAdapterInbound(
            ApplicationPort applicationPort,
            UserMapper userMapper,
            AmqpPort amqpPort,
            ExceptionResolver exceptionResolver) {
        this.applicationPort = applicationPort;
        this.userMapper = userMapper;
        this.amqpPort = amqpPort;
        this.exceptionResolver = exceptionResolver;
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_USER_CREATION_REQUESTED)
    public void subscribeExchangeUserCreationRequested(UserCreationDto userCreationDto) {
        try {
            if(userCreationDto.isValid()){
                applicationPort.create(userMapper.toUser(userCreationDto));
            }else {
                throw new BadRequestException(
                        "Invalid user creation");
            }

        } catch (Exception e) {
            amqpPort.notifyUserOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(userCreationDto));
        }
    }


    @StreamListener(target = BindConfig.SUBSCRIBE_USER_DELETION_REQUESTED)
    public void subscribeExchangeUserDeletionRequested(UserDeletionDto userDeletionDto) {
        try {
            applicationPort.delete(userDeletionDto.getId());
        } catch (Exception e) {
            amqpPort.notifyUserOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(userDeletionDto));
        }
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_USER_UPDATE_REQUESTED)
    public void subscribeExchangeUserUpdateRequested(UserUpdateDto userUpdateDto) {
        try {
            BeanValidator.validate(userUpdateDto);

            User user = userMapper.toUser(userUpdateDto);

            applicationPort.update(user, userUpdateDto.getId());
        } catch (Exception e) {
            amqpPort.notifyUserOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(userUpdateDto));
        }
    }
}
