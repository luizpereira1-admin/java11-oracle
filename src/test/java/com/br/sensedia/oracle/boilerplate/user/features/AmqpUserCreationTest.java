package com.br.sensedia.oracle.boilerplate.user.features;

import com.fasterxml.jackson.core.type.TypeReference;
import com.br.sensedia.oracle.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.dto.UserDto;
import com.br.sensedia.oracle.boilerplate.commons.BrokerResponse;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.domain.UserStatus;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.io.IOException;

import static com.br.sensedia.oracle.boilerplate.commons.headers.DefaultHeader.APP_ID_HEADER_NAME;
import static com.br.sensedia.oracle.boilerplate.commons.headers.DefaultHeader.EVENT_NAME_HEADER_HEADER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class AmqpUserCreationTest extends AbstractUserTest {

  @BeforeEach
  public void setup() {
    repository.deleteAll();
  }

  @Test
  @DisplayName("I want to create a user with success")
  public void createUserSuccessfully() throws IOException {
    UserCreationDto userCreation = new UserCreationDto();

    userCreation.setEmail("carlos.lobato@sensedia.com");
    userCreation.setName("Carlos Lobato");

    Message<UserCreationDto> message =
        MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

    brokerInput.subscribeUserCreationRequested().send(message);

    // DATABASE VALIDATION
    User user = repository.findAll().iterator().next();

    assertThat(user.getId()).isGreaterThanOrEqualTo(1);
    assertThat(user.getEmail()).isEqualTo("carlos.lobato@sensedia.com");
    assertThat(user.getName()).isEqualTo("Carlos Lobato");
    assertThat(user.getStatus()).isEqualTo(UserStatus.ACTIVE);
    assertThat(user.getCreatedAt()).isNotNull();
    assertThat(user.getUpdatedAt()).isNull();

    // NOTIFICATION VALIDATION
    BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserCreated());

    UserDto userResponse = brokerResponse.getPayload(UserDto.class);

    assertThat(user.getId()).isGreaterThanOrEqualTo(1);
    assertThat(userResponse.getEmail()).isEqualTo("carlos.lobato@sensedia.com");
    assertThat(userResponse.getName()).isEqualTo("Carlos Lobato");
    assertThat(userResponse.getStatus()).isEqualTo(UserStatus.ACTIVE.toString());
    assertThat(userResponse.getCreatedAt()).isNotNull();
    assertThat(userResponse.getUpdatedAt()).isNull();

    MessageHeaders headers = brokerResponse.getHeaders();

    assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserCreation");
    assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("oracle-boilerplate");
  }

  @Test
  @DisplayName("I want to create a user without email")
  public void createUserWithoutEmail() throws IOException {
    UserCreationDto userCreation = new UserCreationDto();
    userCreation.setName("Thiago Costa");

    Message<UserCreationDto> message =
            MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test").build();

    brokerInput.subscribeUserCreationRequested().send(message);

    // RESPONSE VALIDATION
    BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

    DefaultErrorResponse<UserCreationDto> response =
            brokerResponse.getPayload(new TypeReference<>() {});

    assertThat(response.getOriginalMessage()).isEqualTo(userCreation);
    assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
    assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
    assertThat(response.getDetail()).isEqualTo("Invalid user creation");
    assertThat(response.getType()).isNull();

    MessageHeaders headers = brokerResponse.getHeaders();

    assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
    assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("oracle-boilerplate");

    // REPOSITORY VALIDATION
    assertThat(repository.findAll()).hasSize(0);

    // NOTIFICATION VALIDATION
    assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
  }

  @Test
  @DisplayName("I want to create a user without name")
  public void createUserWithoutName() throws IOException {
    UserCreationDto userCreation = new UserCreationDto();
    userCreation.setEmail("carlos.lobato@sensedia.com");

    Message<UserCreationDto> message =
        MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test").build();

    brokerInput.subscribeUserCreationRequested().send(message);

    // RESPONSE VALIDATION
    BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

    DefaultErrorResponse<UserCreationDto> response =
        brokerResponse.getPayload(new TypeReference<>() {});

    assertThat(response.getOriginalMessage()).isEqualTo(userCreation);
    assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
    assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
    assertThat(response.getDetail()).isEqualTo("Invalid user creation");
    assertThat(response.getType()).isNull();

    MessageHeaders headers = brokerResponse.getHeaders();

    assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
    assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("oracle-boilerplate");

    // REPOSITORY VALIDATION
    assertThat(repository.findAll()).hasSize(0);

    // NOTIFICATION VALIDATION
    assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
  }

  @Test
  @DisplayName("I want to create a user with invalid email")
  public void createUserWithInvalidEmail() throws IOException {
    UserCreationDto userCreation = new UserCreationDto();
    userCreation.setEmail("carlos.lobato.sensedia.com");
    userCreation.setName("Carlos Lobato");

    Message<UserCreationDto> message =
        MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test").build();

    brokerInput.subscribeUserCreationRequested().send(message);

    // RESPONSE VALIDATION
    BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

    DefaultErrorResponse<UserCreationDto> response =
        brokerResponse.getPayload(new TypeReference<>() {});

    assertThat(response.getOriginalMessage()).isEqualTo(userCreation);
    assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
    assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
    assertThat(response.getDetail()).isEqualTo("Invalid user creation");
    assertThat(response.getType()).isNull();

    MessageHeaders headers = brokerResponse.getHeaders();

    assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
    assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("oracle-boilerplate");

    // REPOSITORY VALIDATION
    assertThat(repository.findAll()).hasSize(0);

    // NOTIFICATION VALIDATION
    assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
  }
}
