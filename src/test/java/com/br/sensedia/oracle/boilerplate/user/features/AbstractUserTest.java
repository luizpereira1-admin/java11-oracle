package com.br.sensedia.oracle.boilerplate.user.features;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.br.sensedia.oracle.boilerplate.user.infrastructure.amqp.config.BrokerInput;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.amqp.config.BrokerOutput;

import com.br.sensedia.oracle.boilerplate.user.services.UserService;
import com.br.sensedia.oracle.boilerplate.commons.MessageCollectorCustom;
import com.br.sensedia.oracle.boilerplate.user.domain.User;
import com.br.sensedia.oracle.boilerplate.user.infrastructure.ports.RepositoryPort;
import io.micrometer.core.instrument.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractUserTest {

  static final long USER_ID_VALID = 1;

  static final long USER_ID_NOT_FOUND = 999;

  @Autowired RepositoryPort repository;

  @Autowired ObjectMapper mapper;

  @Autowired TestRestTemplate request;

  @Autowired MessageCollectorCustom collector;

  @Autowired BrokerOutput brokerOutput;

  @Autowired BrokerInput brokerInput;

  @Autowired private UserService userApplication;

  @Value("classpath:users.json")
  private Resource usersJson;

  void loadDatabase() throws IOException {
    String json = loadData(usersJson);
    List<User> users = mapper.readValue(json, new TypeReference<List<User>>() {});
    repository.saveAll(users);
  }

  String loadData(Resource resource) throws IOException {
    return IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);
  }

  boolean isUUID(String uuid) {
    return uuid.matches(
        "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
  }

  void injectDatabaseError() {
    RepositoryPort failingRepository = mock(RepositoryPort.class);
    ReflectionTestUtils.setField(userApplication, "repository", failingRepository);
  }

  void undoDatabaseError() {
    ReflectionTestUtils.setField(userApplication, "repository", repository);
  }
}
