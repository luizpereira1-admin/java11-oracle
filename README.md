### Guides
The following guides illustrate how to use some features concretely:

* [Messaging with RabbitMQ](https://spring.io/guides/gs/messaging-rabbitmq/)
* [Creating Local Oracle Database](https://imasters.com.br/banco-de-dados/deploy-de-um-banco-oracle-em-um-container-docker-linux-e-conexao-via-sql-developer)
* [Spring Boot with Oracle](https://www.javaguides.net/2020/01/spring-boot-hibernate-oracle-crud-example.html)
* [Swagger for Spring](http://springfox.github.io/springfox/docs)